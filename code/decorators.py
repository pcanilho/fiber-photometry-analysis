import ipywidgets as widgets
from libraries.widgets import gen_export_plt_as_png


def to_ipy_widget(func):
    def wrapper(*args, **kwargs):
        out = widgets.Output()
        with out:
            func(*args, **kwargs)
        return out

    return wrapper


def exportable_plot(func):
    def wrapper(*args, **kwargs):
        gen_export_plt_as_png(func(*args, **kwargs))
    return wrapper
