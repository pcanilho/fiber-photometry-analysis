# Int
from decorators import to_ipy_widget, exportable_plot
import libraries.widgets
# Ext
from IPython.display import display
import os.path
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from typing import Callable, Any
from scipy import optimize, special
from scipy.signal import find_peaks
from scipy.fft import fft, fftfreq
import ipywidgets as widgets
from ipywidgets import interact
from scipy import signal
import seaborn as sns


def default_exponential_fit(x, m, t, b):
    return m * np.exp(special.expit(-t * x)) + b


# noinspection DuplicatedCode
class Analysis:
    # Attributes
    filepath: str
    title: str
    # Data
    data: pd.DataFrame
    fitted_data: pd.DataFrame
    filtered_windowed_data: pd.DataFrame
    decimated_signal: pd.DataFrame
    f_min: int
    f_max: int
    # Fitted data
    fitted_exp: Any
    tau_sec: float
    m: Any
    t: Any
    b: Any
    # Controllers
    f0_controller: widgets.IntRangeSlider
    f0_value: Any
    # Defaults
    time_raw = ('---', 'Time(s)')
    column_ft = 'F(t)'
    column_time = 'Time(s)'

    column_b = ('Analog In. | Ch.1', 'AIn-1 - Dem (AOut-1)')
    column_c = ('Analog In. | Ch.1', 'AIn-1 - Dem (AOut-2)')

    #####################################
    # Constructor
    def __init__(self,
                 csv_filepath: str,
                 header=(0, 1),
                 column_time: str = column_time,
                 time_raw: str = time_raw) -> None:

        # Attributes
        self.filepath = csv_filepath
        self.title = os.path.basename(csv_filepath)
        # Override defaults
        self.column_time = column_time
        self.time_raw = time_raw
        # Read data
        self.data = pd.read_csv(csv_filepath, header=list(header))
        # Pre-Process data
        self.data.drop(self.data.filter(regex="Unnamed"), axis=1, inplace=True)
        self.data[self.column_time] = self.data[self.time_raw]
        self.data.drop(self.data.filter(regex='---'), axis=1, inplace=True)

        # Automatic calculations
        self.calculate_ft()
        try:
            self.calculate_fit()
        except RuntimeError:
            ######################
            # Handle exceptions
            self.fitted_data = self.data
            pass

    def signal(self) -> pd.DataFrame:
        return self.data

    #####################################
    # Calculations
    def calculate_ft(self,
                     column_name: str = column_ft,
                     a: any = column_b,
                     b: any = column_c) -> None:
        self.column_ft = column_name
        self.data.dropna(subset=[a, b], inplace=True)
        self.data[column_name] = self.data[a] - self.data[b]
        self.f_min = int(self.data[self.column_time].min())
        self.f_max = int(self.data[self.column_time].max())

    def calculate_fit(self,
                      fitting_fn: Callable = default_exponential_fit, sample_rate: int = 20_000_000,
                      inspect=False):
        # perform the fit
        p0 = (0.970, 0.1, 1)  # start with values near those we expect
        params, cv = optimize.curve_fit(fitting_fn, self.data[self.column_time], self.data[self.column_ft], p0)
        self.m, self.t, self.b = params
        self.tau_sec = (1 / self.t) / sample_rate

        # determine quality of the fit
        squared_diffs = np.square(
            self.data[self.column_ft] - fitting_fn(self.data[self.column_time], self.m, self.t, self.b))
        squared_diffs_from_mean = np.square(self.data[self.column_ft] - np.mean(self.data[self.column_ft]))
        r_squared = 1 - np.sum(squared_diffs) / np.sum(squared_diffs_from_mean)

        self.fitted_exp = fitting_fn(self.data[self.column_time], self.m, self.t, self.b)

        # fit the original data
        self.fitted_data = self.data[[self.column_time, self.column_ft]].copy()
        self.fitted_data[self.column_ft] = self.fitted_data[self.column_ft] - self.fitted_exp

        if inspect:
            print(f"R² = {r_squared}")

    #####################################
    # Exporters

    # noinspection PyTypeChecker
    def export(self, option='csv', pattern='-analysed', base_dir='output/csv'):
        filename = format_file_name(self.filepath, pattern)
        path = f"{base_dir}/{filename}"
        Path(base_dir).mkdir(parents=True, exist_ok=True)
        self.decimated_signal.to_csv(path, index=False)

    #####################################
    # Plots

    @to_ipy_widget
    @exportable_plot
    def plot_ft(self) -> list[Any]:
        plot = self.data.plot(x=self.column_time, y=self.column_ft, figsize=(35, 10))
        plt.legend(loc='best', fontsize='xx-large')
        plot.set_xlim(self.f_min, self.f_max)
        plt.show()
        return [plot, f"{self.title}-original.png"]

    @to_ipy_widget
    # @exportable_plot
    def plot_fitted_preview(self, inspect=False) -> list[Any]:
        plt.subplots(figsize=(35, 10))
        plt.plot(self.data[self.column_time], self.data[self.column_ft], '.', label="data")
        if hasattr(self, 'fitted_exp'):
            plt.plot(self.data[self.column_time], self.fitted_exp, '--', label="fitted", lw=2)
        fig = plt.Figure()
        plt.xlim(self.f_min, self.f_max)
        plt.title("Fitted Exponential Curve")
        plt.legend(loc='best', fontsize='xx-large')
        plt.show()
        if inspect:
            # Inspect the parameters
            print(f"Y = {self.m} * e^(-{self.t} * x) + {self.b}")
            print(f"Tau = {self.tau_sec * 1e6} µs")

        return [fig, f"{self.title}-fitted-preview.png"]

    @to_ipy_widget
    # @exportable_plot
    def plot_fitted(self) -> list[Any]:
        plt.subplots(figsize=(35, 10))
        plot = plt.plot(self.data[self.column_time], self.fitted_data[self.column_ft], '-',
                        label='original.sub(fitted)')
        if hasattr(self, 'fitted_exp'):
            plt.axhline(y=0, color='C1', linestyle='dashed', label='reference', lw=2)
        plt.legend(loc='best', fontsize='xx-large')
        plt.xlim(self.f_min, self.f_max)
        plt.show()
        return [plot, f"{self.title}-fitted.png"]

    @to_ipy_widget
    @exportable_plot
    def plot_fitted_dt(self) -> list[Any]:
        fdt = self.fitted_data.copy()
        fdt[self.column_ft] = fdt[self.column_ft].apply(lambda x: (x - self.f0_value) / self.f0_value)
        plot = fdt.plot(self.column_time, self.column_ft, label="FF'(t)", figsize=(35, 10))
        plt.xlim(self.f_min, self.f_max)
        plt.legend(loc='best', fontsize='xx-large')
        plt.show()
        return [plot, f"{self.title}-fdt.png"]

    #####################################
    # Controllers
    @to_ipy_widget
    def controller_ff0(self):
        # Create widget
        self.f0_controller = widgets.IntRangeSlider(
            value=[(self.f_max - self.f_min) / 3, (self.f_max - self.f_min) / 2],
            min=self.f_min,
            max=self.f_max,
            step=1,
            description="<b>F(0) range</b>",
            disabled=False,
            continuous_update=True,
            orientation='horizontal',
            readout=True,
            readout_format='d',
            layout={'width': '90%'}
        )

        # Callback
        def update(c):
            # Calculate the initial window
            windowed_data = self.fitted_data[
                self.fitted_data[self.column_time].between(self.f0_controller.value[0], self.f0_controller.value[1])]
            self.f0_value = windowed_data[self.column_ft].mean()
            w_max = windowed_data[self.column_ft].max()
            plt.subplots(figsize=(35, 10))
            plt.plot(windowed_data[self.column_time], windowed_data[self.column_ft], label='FF(0)')
            plt.xlim(c[0], c[1])
            if hasattr(self, 'fitted_exp'):
                plt.axhline(y=0, color='C1', linestyle='dashed', label='reference', lw=2)
            plt.annotate('FF(0): %.20f' % self.f0_value, (c[0] + (c[1] - c[0]) / 50, w_max - w_max / 300),
                         color='#eb4034',
                         fontsize='xx-large')
            plt.legend(loc='best', fontsize='xx-large')
            plt.show()

        return interact(update, c=self.f0_controller)

    @to_ipy_widget
    def controller_fft(self):
        # Create widgets
        fdt_window = libraries.widgets.default_filtered_fdt_window
        fdt_window.min = self.f_min
        fdt_window.max = self.f_max
        fdt_window.value = (self.f_min, self.f_max)

        butter_filter_sampling_freq = libraries.widgets.default_butter_filter_sampling_freq
        butter_filter_cutoff = libraries.widgets.default_butter_filter_cutoff
        peak_height_threshold = libraries.widgets.default_peak_height_threshold
        decimation_settings = libraries.widgets.gen_decimation_chooser(
            initial_value=0, description='Decimation step')

        # Initialisation
        fdt = self.fitted_data.copy()
        fdt[self.column_ft] = fdt[self.column_ft].apply(lambda x: (x - self.f0_value) / self.f0_value)
        column_delta_ft = 'ΔF/F'

        def gen_filtered_fft(sampling, cutoff, window, peak, decimation):
            # Initialisation
            b, a = signal.butter(3, cutoff, fs=sampling)
            filtered_fdt = signal.filtfilt(b, a, fdt[self.column_ft])
            filtered_df = pd.DataFrame(data=zip(self.data[self.column_time], filtered_fdt),
                                       columns=[self.column_time, column_delta_ft])
            self.filtered_windowed_data = filtered_df[
                filtered_df[self.column_time].between(window[0], window[1])]

            # Filter the Fitted signal FF(t) -> FFF(t)
            plt.subplots(figsize=(35, 10))
            plt.plot(fdt[self.column_time], fdt[self.column_ft], label="FF'(t)")
            plt.plot(self.filtered_windowed_data[self.column_time], self.filtered_windowed_data[column_delta_ft],
                     label="Filtered")
            plt.title("Filtered + Fitted F'(t))")
            plt.xlim(window[0], window[1])
            plt.legend(loc='best', fontsize='xx-large')
            plt.show()

            # Heatmap
            snap = self.filtered_windowed_data.copy()
            snap[self.column_time] = snap[self.column_time].apply(int)
            snap = snap.groupby(self.column_time).agg('mean').transpose()
            plt.subplots(figsize=(35, 7))
            sns.set(font_scale=1.6)
            sns.heatmap(snap, robust=True, square=False, yticklabels=True, xticklabels=300,
                        cbar_kws={'orientation': 'horizontal'})
            plt.title("Filtered + Fitted F'(t) :: Heatmap")
            plt.ylabel(self.column_ft)
            plt.show()

            # Z-Score
            z_snap = self.filtered_windowed_data.copy()
            column_z_score = 'Z-Score'
            z_snap[column_z_score] = \
                (z_snap[column_delta_ft] - z_snap[column_delta_ft].mean()) / z_snap[column_delta_ft].std(ddof=1)
            plt.subplots(figsize=(35, 7))
            plt.plot(z_snap[self.column_time], z_snap[column_z_score], label=column_z_score, color='C4')
            plt.title("Filtered + Fitted F'(t) :: Z-Score")
            plt.xlim(window[0], window[1])
            plt.ylabel('Z-Score')
            plt.show()

            # Add to the exported DataFrame
            self.filtered_windowed_data = self.filtered_windowed_data.join(z_snap[column_z_score])

            # Peaks
            p_snap = z_snap.copy()
            column_m_avg = 'mAVG'
            m_avg_step = 1000

            p_snap[column_m_avg] = p_snap[column_z_score].rolling(m_avg_step).mean()
            list_df = [p_snap[i:i + m_avg_step] for i in range(0, p_snap.shape[0], m_avg_step)]
            p_series = []
            it = 0
            for df in list_df:
                peaks, _ = find_peaks(df[column_z_score], height=df[column_z_score].mean() + peak)
                p_series.extend(pd.Series(peaks).apply(lambda x: x + it))
                it += m_avg_step

            peak_filtered = p_snap.iloc[p_series]
            plt.subplots(figsize=(35, 7))
            plt.plot(p_snap[self.column_time], p_snap[column_z_score], label="Filtered FF'(t)")
            plt.plot(p_snap[self.column_time], p_snap[column_m_avg], label="mAVG(FF'(t), %d)" % m_avg_step)
            plt.plot(peak_filtered[self.column_time], peak_filtered[column_z_score], 'x', label='Peak', color='C3')
            plt.xlim(window[0], window[1])
            plt.title('Peak detection :: Z-Score :: P(t)')
            plt.legend(loc='best', fontsize='medium')
            plt.show()

            #########
            # Export Peaks
            export_peaks = libraries.widgets.gen_export_plt_as_csv([peak_filtered[[self.column_time, column_z_score]],
                                                                    f"{self.title}-peaks.csv"],
                                                                   description="Export peaks")

            display(export_peaks)
            #########

            # FFT
            fft_req_snap = self.filtered_windowed_data.copy()
            y = fft_req_snap[column_delta_ft].tolist()
            # Number of sample points
            N = 512
            # sample spacing
            T = 1.0 / sampling
            yf = fft(y)
            xf = fftfreq(N, T)[:N // 2]
            plt.subplots(figsize=(35, 7))
            plt.plot(xf, 2.0 / N * np.abs(yf[0:N // 2]))
            plt.grid()
            plt.title('Fast Fourier Transform')
            plt.xlim(xf.min(), xf.max())
            plt.ylabel('FFT')
            plt.show()

            # Show decimated FF'(t)
            self.decimated_signal = self.filtered_windowed_data.copy()
            if decimation > 0:
                self.decimated_signal = self.decimated_signal.iloc[::decimation, :]
            plt.subplots(figsize=(35, 7))
            plt.plot(self.decimated_signal[self.column_time], self.decimated_signal[column_delta_ft],
                     label="Decimated FF'(t)")
            plt.title("Decimated FF'(t)")
            plt.xlim(self.f_min, self.f_max)
            plt.show()

        return interact(gen_filtered_fft,
                        sampling=butter_filter_sampling_freq, cutoff=butter_filter_cutoff,
                        window=fdt_window, peak=peak_height_threshold, decimation=decimation_settings)


#################################
# Utils
desired_pattern = '-analysed'


# noinspection DuplicatedCode
def export(analysis_list: [Analysis]):
    if not analysis_list:
        return widgets.Text(value="No analysis has been computed yet...", disabled=True)
    # Render the analysis that are going to be exported
    analysis_to_be_exported = libraries.widgets.gen_list_viewer(options=[a.title for a in analysis_list])

    # Render export option selection
    export_options_toggle = libraries.widgets.default_export_options_toggle

    def pattern_changed(v):
        global desired_pattern
        desired_pattern = v

    def option_callback(v):
        [a.export(option=v, pattern=desired_pattern) for a in analysis_list]

    global desired_pattern
    export_file_pattern = libraries.widgets.gen_export_file_name_text(pattern_changed,
                                                                      placeholder=f"Defaults to [{desired_pattern}]",
                                                                      description='Pattern')
    export_all_button = libraries.widgets.gen_export_button(callback=option_callback, description='Export all')
    return widgets.VBox(
        [widgets.Box([analysis_to_be_exported], layout={'padding': '10px'}), widgets.GridBox(
            [export_options_toggle, export_file_pattern],
            layout=widgets.Layout(grid_template_columns="repeat(3, 300px)")),
         widgets.Box([export_all_button], layout={'padding': '10px'})])


# noinspection DuplicatedCode
def plot_all(analysis_list: [Analysis], func: str) -> widgets.Tab:
    children = [f() for f in [getattr(a, func) for a in analysis_list]]
    tabbed_content = widgets.Tab(layout={'width': '100%'})
    tabbed_content.children = children
    [tabbed_content.set_title(title=f"🗎 {a.title}", index=i) for i, a in enumerate(analysis_list)]
    return tabbed_content


def format_file_name(name: str, pattern: str) -> str:
    f_name, f_ext = os.path.basename(name).split('.')
    return f"{f_name}{pattern}.{f_ext}"
