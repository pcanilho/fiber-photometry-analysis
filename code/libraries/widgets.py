import ipywidgets as widgets
import matplotlib.figure
from IPython.display import display
from pathlib import Path

default_decimation_steps = widgets.IntRangeSlider(
    step=1,
    description="<b>Decimation</b>",
    disabled=False,
    continuous_update=True,
    orientation='horizontal',
    readout=True,
    readout_format='d',
    layout={'width': '90%'}
)

default_filtered_fdt_window = widgets.IntRangeSlider(
    step=1,
    description="<b>Window</b>",
    disabled=False,
    continuous_update=True,
    orientation='horizontal',
    readout=True,
    readout_format='d',
    layout={'width': '90%'}
)

default_butter_filter_sampling_freq = widgets.IntText(
    value=100,
    description='LPF <b>sampling</b>',
    continuous_update=True,
    disabled=False,
)

default_cutoff = default_butter_filter_sampling_freq.value / 10

default_butter_filter_cutoff = widgets.FloatSlider(
    value=default_cutoff,
    max=default_butter_filter_sampling_freq.value,
    step=0.01,
    description='LPF <b>cutoff</b>',
    disabled=False,
    continuous_update=True,
    orientation='horizontal',
    readout=True,
    readout_format='.2f',
    layout={'width': '90%'}
)


def update_butter_filter_cutoff(butter_filter, filtered_fdt_window, v):
    butter_filter.max = v
    display(butter_filter, filtered_fdt_window)


default_peak_height_threshold = widgets.FloatText(
    value=3,
    description='Peak <b>distance</b>',
    continuous_update=True,
    disabled=False,
    style={'description_width': 'initial'}
)

default_export_options_toggle = widgets.ToggleButtons(
    options=['CSV', 'GraphPad'],
    description='Option:',
    disabled=False,
    button_style='info',  # 'success', 'info', 'warning', 'danger' or ''
    tooltips=[
        'The CSV format will be used to export results',
        'The PZFX GraphPad format will be used to export results'],
    icons=['🗎', 'G']
)


def gen_list_viewer(description='', options=None) -> widgets.SelectMultiple:
    return widgets.SelectMultiple(
        options=options,
        value=[],
        description=description,
        disabled=True,
        style=dict(description_width='initial')
    )


def gen_decimation_chooser(initial_value=3000, placeholder='', description='') -> widgets.IntText:
    return widgets.IntText(
        value=initial_value,
        min=0,
        placeholder=placeholder,
        description=description,
        disabled=False,
        style=dict(description_width='initial')
    )


def gen_export_file_name_text(callback, initial_value='', placeholder='', description='') -> widgets.Text:
    w = widgets.Text(
        value=initial_value,
        placeholder=placeholder,
        description=description,
        disabled=False,
        style=dict(description_width='initial')
    )
    w.on_msg(callback=callback)
    return w


def gen_export_button(callback, description='Export all') -> widgets.Button:
    w = widgets.Button(description=description,
                       style=dict(description_width='initial'), disabled=False)
    w.on_click(callback)
    return w


def gen_export_plt_as_png(config, base_dir='output/images', description='Export as PNG'):
    preview = widgets.Text(placeholder=config[1])

    def on_name_changed(msg):
        nonlocal config
        config[1] = msg

    def export_dataframe_as_png(_):
        print(type(config[0]))
        path = f"{base_dir}/{config[1]}"
        Path(base_dir).mkdir(parents=True, exist_ok=True)
        # Detect plot type
        if type(config[0]) == matplotlib.figure.Figure:
            figure = config[0]
        else:
            figure = config[0].get_figure()
        figure.savefig(path)

    preview.on_msg(on_name_changed)
    export_button = gen_export_button(callback=export_dataframe_as_png, description=description)
    display(widgets.VBox([preview, export_button]))


def gen_export_plt_as_csv(config, base_dir='output/csv', description='Export as CSV'):
    preview = widgets.Text(placeholder=config[1])

    def on_name_changed(msg):
        nonlocal config
        config[1] = msg

    def export_dataframe_as_csv(_):
        path = f"{base_dir}/{config[1]}"
        Path(base_dir).mkdir(parents=True, exist_ok=True)
        config[0].to_csv(path)

    preview.on_msg(on_name_changed)
    export_button = gen_export_button(callback=export_dataframe_as_csv, description=description)
    display(widgets.VBox([preview, export_button]))
