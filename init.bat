echo "Setting up conda environment..."
call conda create -n jupyter -c conda-forge ipyfilechooser  markdown seaborn ipywidgets jupyterlab pandas matplotlib==3.4.2
call conda activate jupyter
call jupyter nbextension install --user --py widgetsnbextension
call jupyter nbextension enable --user --py widgetsnbextension
call jupyter labextension install @jupyter-widgets/jupyterlab-manager
call jupyter-lab
